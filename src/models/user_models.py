from src.db import db


class UserModel(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    username = db.Column(db.String(20), nullable=False)
    password_hash = db.Column(db.Text, nullable=False)
    first_name = db.Column(db.String(40), nullable=False)
    last_name = db.Column(db.String(40), nullable=False)
    country = db.Column(db.String(40), nullable=False)
    email = db.Column(db.String(40), nullable=False)
    dob = db.Column(db.String(10), nullable=False)


class TestModel(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)

class TestSecondModel(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
