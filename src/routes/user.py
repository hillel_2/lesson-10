from flask import render_template, Blueprint

from src.db import db
from src.models.user_models import UserModel

user_route = Blueprint('user', __name__, url_prefix='/user')

@user_route.route('/<user_id>')
def user(user_id):
    user_model = db.session.query(
        UserModel.username,
        UserModel.first_name,
        UserModel.last_name,
        UserModel.email,
        UserModel.country,
        UserModel.dob
    ).filter(
        UserModel.id == user_id
    ).one()
    return render_template('user.html', user=user_model)


@user_route.route('/')
def main():
    users = db.session.query(UserModel).all()
    return render_template('index.html', users=users, len=len)