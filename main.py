from src.app import app
from src.db import db

if __name__ == '__main__':
    db.create_all(app=app)
    app.run(port=8080)